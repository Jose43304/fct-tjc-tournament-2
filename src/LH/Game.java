package LH;

import cern.colt.matrix.tdouble.DoubleFactory2D;
import cern.colt.matrix.tdouble.DoubleMatrix1D;
import cern.colt.matrix.tdouble.DoubleMatrix2D;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Game {
    private DoubleMatrix2D A, B;

    private DoubleMatrix2D rowTableau, columnTableau;

    private Set<Integer> labels;

    public Game(DoubleMatrix2D a, DoubleMatrix2D b) {
        A = a;
        B = b;
    }

    private void normalizeMatrix(DoubleMatrix2D m) {
        double[] loc = m.getMinLocation();

        if (loc[0] <= 0)
            for (int row = 0; row < m.rows(); row++)
                for (int column = 0; column < m.columns(); column++)
                    m.setQuick(row, column, m.getQuick(row, column) + Math.abs(loc[0]) + 1.0);
    }

    private void buildColumnTableau() {
        DoubleFactory2D factory2D = DoubleFactory2D.dense;

        DoubleMatrix2D identity = factory2D.identity(A.rows());
        DoubleMatrix2D vector = factory2D.make(A.rows(), 1, 1.0);

        DoubleMatrix2D[][] parts = {{ identity, A, vector}};

        columnTableau = factory2D.compose(parts);
    }

    private void buildRowTableau() {
        DoubleFactory2D factory2D = DoubleFactory2D.dense;
        DoubleMatrix2D tB = B.viewDice();

        DoubleMatrix2D identity = factory2D.identity(tB.rows());
        DoubleMatrix2D vector = factory2D.make(tB.rows(), 1, 1.0);

        DoubleMatrix2D[][] parts = {{ tB, identity, vector}};

        rowTableau = factory2D.compose(parts);
    }

    private void buildLabels() {
        labels = IntStream.range(0, A.rows() + A.columns()).boxed().collect(Collectors.toSet());
    }

    private Set<Integer> getNonBasicVars(DoubleMatrix2D tableau) {
        Set<Integer> r = new TreeSet<>();

        for (int c = 0; c < tableau.columns() - 1; c++) {
            DoubleMatrix1D column = tableau.viewColumn(c);

            if (column.cardinality() != 1)
                r.add(c);
        }

        return r;
    }

    private Integer getPivotRowIndex(DoubleMatrix2D tableau, int column_index) {
        double maxVal = Double.MIN_VALUE;
        int maxIdx = -1;

        DoubleMatrix1D column = tableau.viewColumn(column_index);
        DoubleMatrix1D lastColumn = tableau.viewColumn(tableau.columns() - 1);
        for (int i = 0; i < tableau.rows(); i++) {
            double v = column.getQuick(i) / lastColumn.getQuick(i);
            if (v >= maxVal /*|| Double.isNaN(v)*/) {
                maxVal = v;
                maxIdx = i;
            }
        }

        return maxIdx;
    }

    private Set<Integer> pivotTableau(DoubleMatrix2D tableau, int column_index) {
        Set<Integer> originalLabels = getNonBasicVars(tableau);
        Integer pivotRowIndex = getPivotRowIndex(tableau, column_index);
        double pivotElement = tableau.getQuick(pivotRowIndex, column_index);

        DoubleMatrix1D pivotRow = tableau.viewRow(pivotRowIndex);

        for (int i = 0; i < tableau.rows(); i++) {
            if (i != pivotRowIndex) {
                DoubleMatrix1D row = tableau.viewRow(i);

                double row_column_val = row.getQuick(column_index);
                for (int j = 0; j < row.size(); j++) {
                    row.setQuick(j,
                            row.getQuick(j) * pivotElement
                                    - pivotRow.getQuick(j) * row_column_val
                    );
                }
            }
        }

        Set<Integer> vars = getNonBasicVars(tableau);
        vars.removeAll(originalLabels);

        return vars;
    }

    private List<Double> getStrategy(
            DoubleMatrix2D tableau,
            Set<Integer> tableauxLabels,
            List<Integer> strategyLabels) {
        List<Double> strategy = new ArrayList<>();

        for (Integer c : strategyLabels) {
            if (tableauxLabels.contains(c)) {
                DoubleMatrix1D column = tableau.viewColumn(c);
                for (int i = 0; i < column.size(); i++) {
                    double r = column.getQuick(i);
                    if (r != 0.0)
                        strategy.add(tableau.getQuick(i, tableau.columns() - 1) / r);
                }
            } else {
                strategy.add(0.0);
            }
        }

        Double sum = strategy.stream().mapToDouble(Double::doubleValue).sum();

        return strategy.stream().map(x -> x / sum).collect(Collectors.toList());
    }



    private Pair LemkeHowson(Integer label) {
        buildColumnTableau();
        buildRowTableau();

        buildLabels();

        DoubleMatrix2D[] tableauCycle = {
                rowTableau,
                columnTableau
        };
        int tableauIndex = getNonBasicVars(rowTableau).contains(label) ? 0 : 1;

        Set<Integer> start_label = pivotTableau(tableauCycle[tableauIndex], label);
        tableauIndex = (tableauIndex + 1) % 2;

        Set<Integer> current_labels = getNonBasicVars(rowTableau);
        current_labels.addAll(getNonBasicVars(columnTableau));
        while(!current_labels.containsAll(labels) && start_label.size() > 0) {
            start_label = pivotTableau(tableauCycle[tableauIndex], start_label.iterator().next());
            tableauIndex = (tableauIndex + 1) % 2;

            current_labels = getNonBasicVars(rowTableau);
            current_labels.addAll(getNonBasicVars(columnTableau));
        }

        List<Double> rowStrategy = getStrategy(
                rowTableau,
                getNonBasicVars(columnTableau),
                IntStream.range(0, A.rows()).boxed().collect(Collectors.toList())
        );

        List<Double> colStrategy = getStrategy(
                columnTableau,
                getNonBasicVars(rowTableau),
                IntStream.range(A.rows(), A.rows() + A.columns()).boxed().collect(Collectors.toList())
        );

        if (rowStrategy.size() != A.rows() && colStrategy.size() != A.rows())
            System.out.println("Possible degenerate game detected.");

        return new Pair(rowStrategy, colStrategy);
    }

    public Set<Pair> enumerateLemkeHowson() {
        normalizeMatrix(A);
        normalizeMatrix(B);

        int numLabels = A.rows() + A.columns();
        Set<Pair> results = new HashSet<>(numLabels);

        for (int i = 0; i < numLabels; i++)
            results.add(LemkeHowson(i));

        return results;
    }
}
