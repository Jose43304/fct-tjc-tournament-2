package LH;

import java.util.Set;

import cern.colt.matrix.tdouble.DoubleMatrix2D;
import cern.colt.matrix.tdouble.impl.DenseDoubleMatrix2D;

public class Test {
    public static void main(String[] args)
    {
        /* A       B
         * [3, 3]  [3, 2]
         * [2, 5]  [2, 6]
         * [0, 6]  [3, 1]
         */


        /*double[][] a = new double[][] {
                {0, 0},
                {0, 0}
        };
        double[][] b = new double[][] {
                {0, 2},
                {0, 0}
        };*/
        /*double[][] a = new double[][] {
                {2, 5},
                {5, 2},
                {5, 2}
        };
        double[][] b = new double[][] {
                {9, 4},
                {4, 9},
                {4, 9}
        };*/
        double[][] a = new double[][] {
                {8, 1, 0},
                {1, 2, 3}
        };
        double[][] b = new double[][] {
                {2, 7, 0},
                {2, 0, 8}
        };

        DoubleMatrix2D A = new DenseDoubleMatrix2D(a);
        DoubleMatrix2D B = new DenseDoubleMatrix2D(b);

        Game game = new Game(A, B);
        Set<Pair> result = game.enumerateLemkeHowson();

        for (Pair p : result)
            System.out.println(p);
    }
}