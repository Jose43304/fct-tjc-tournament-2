package LH;

import java.util.Arrays;
import java.util.List;

public class Pair {
    private List<Double> a;
    private List<Double> b;

    public Pair(List<Double> A, List<Double> B) {
        this.a = A;
        this.b = B;
    }

    public List<Double> getA() {
        return this.a;
    }

    public List<Double> getB() {
        return this.b;
    }

    @Override
    public String toString() {
        return Arrays.toString(a.toArray()) + " " + Arrays.toString(b.toArray());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + a.hashCode();
        result = prime * result + b.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Pair))
            return false;

        Pair o = (Pair)obj;
        if (a.size() != o.a.size() || b.size() != o.b.size())
            return false;

        for (int i = 0; i < a.size(); i++)
            if (!a.get(i).equals(o.a.get(i)))
                return false;

        for (int i = 0; i < b.size(); i++)
            if (!b.get(i).equals(o.b.get(i)))
                return false;

        return true;
    }
}