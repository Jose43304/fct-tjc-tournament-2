package lp;

import com.winvector.lp.LPException;

public class Start {

	public static void main(String[] args) {
		double[][] payoff = new double[][]{{30, -10, 20}, {-10, 20, -20}};
		//TwoPersonZeroSumGame game = new TwoPersonZeroSumGame(payoff);
		LinearProgramming lp = new LinearProgramming();
		try {
			LinearProgramming.linearProgrammingExample();
		} catch (LPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*IteratedDominance problem = new IteratedDominance();
		try {
			problem.iteratedDominanceExample();
			problem.getProblemSol();
		} catch (LPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
//		//Problem 2
//		
//		int[] utilFunc = {3, 2, 7};
//		int[][] constraints = {{-1, 1, 0, 1}, {-1, 1, 0, -1}, {2, -1, 1, -1}};
//		
//		LinearProgrammingProblem problem = LinearProgramming.setProblem(/*3, 3, utilFunc, constraints*/);
//		LinearProgramming.showProblem(problem);
//		try {
//			LinearProgramming.solveProblem(problem);
//		} catch (LPException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if (problem.feasible) {
//			System.out.println("The problem is feasible!");
//			if (problem.bounded) {
//				System.out.println("The problem is bounded!");
//				LinearProgramming.showSolution(problem);
//			}
//			else System.out.println("The problem is unbounded!");
//		}
//		else System.out.println("The problem is infeasible!");
	}

}
