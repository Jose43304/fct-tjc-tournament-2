package lp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.winvector.linalg.DenseVec;
import com.winvector.linalg.Matrix;
import com.winvector.linalg.colt.NativeMatrix;
import com.winvector.lp.LPEQProb;
import com.winvector.lp.LPException;
import com.winvector.lp.LPException.LPMalformedException;



public class IteratedDominance {

	NormalFormProblem problem;
	
	public  void iteratedDominanceExample() throws LPException {
		String[] labelsP1 = new String[]{"Top", "Bottom"};
		String[] labelsP2 = new String[]{"Left", "Middle", "Right"};

		int[][] u1 = new int[labelsP1.length][labelsP2.length];
		u1[0][0] = 30; u1[0][1] = -10; u1[0][2] = 20;
		u1[1][0] = -10; u1[1][1] = 20; u1[1][2] = -20;

		int[][] u2 = new int[labelsP1.length][labelsP2.length];
		u2[0][0] = -30; u2[0][1] = 10; u2[0][2] = -20;
		u2[1][0] = 10; u2[1][1] = -20; u2[1][2] = 20;


		problem = new NormalFormProblem(labelsP1, labelsP2, u1, u2);
	}

	public void getProblemSol() throws LPException {
		double[][] moves = problem.getMixedNashEquilibrium();
		double[] movesP1 = moves[0];
		double[] movesP2 = moves[1];

		for (int i = 0; i < movesP1.length; i++){
			System.out.print(movesP1[i] +" ");
		}System.out.println();

		for (int i = 0; i < movesP2.length; i++){
			System.out.print(movesP2[i] +" ");
		}System.out.println();
	}

	
		
	/*public static NormalFormProblem exampleNormalFormProblem(){
		NormalFormProblem problem = new NormalFormProblem();
		problem.rowMoves = new ArrayList<String>();
		problem.colMoves = new ArrayList<String>();	
		
	    problem.rowMoves.add("Top");
	    problem.rowMoves.add("Bottom");
	    problem.colMoves.add("Left");
	    problem.colMoves.add("Middle");
	    problem.colMoves.add("Right");
		boolean[] pRow = new boolean[2];
		for (int i=0;i<pRow.length; i++) pRow[i] = true;
		boolean[] pCol = new boolean[3];
		for (int j=0;j<pCol.length; j++) pCol[j] = true;
		double[][] u2 = new double[2][3];
		u2[0][0] = 8.0; u2[0][1] = 0.0; u2[0][2] = 2.0;
		u2[1][0] = 0.0; u2[1][1] = 7.0; u2[1][2] = 2.0;
		double[][] u1 = new double[2][3];
		u1[0][0] = 3.0; u1[0][1] = 2.0; u1[0][2] = 1.0;
		u1[1][0] = 0.0; u1[1][1] = 1.0; u1[1][2] = 8.0;
		
		problem.pCol = pCol;
		problem.pRow = pRow;
		problem.u1 = u1;
		problem.u2 = u2;
		
		return problem;
		
	}*/
}




