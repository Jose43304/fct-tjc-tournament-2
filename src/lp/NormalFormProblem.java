package lp;

import com.winvector.lp.LPException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by pedroafonso at 2019-06-06
 */

public class NormalFormProblem {

    private List<String> rowMoves;
    private List<String> colMoves;
    public boolean[] pRow, pCol;
    public double[][] u1, u2;

    public NormalFormProblem(String[] labelsP1, String[] labelsP2, int[][] U1, int[][] U2) throws LPException {
        rowMoves = new ArrayList<>();
        colMoves = new ArrayList<>();

        for(int i = 0; i < labelsP1.length; i++){
            rowMoves.add(labelsP1[i]);
        }

        for(int j = 0; j < labelsP2.length; j++){
            colMoves.add(labelsP2[j]);
        }

        pRow = new boolean[labelsP1.length];
        Arrays.fill(pRow, true);
        pCol = new boolean[labelsP2.length];
        Arrays.fill(pCol, true);

        u1 = new double[pRow.length][pCol.length];
        for(int r = 0; r < pRow.length; r++){
            for(int c = 0; c < pCol.length; c++){
                u1[r][c] = U1[r][c];
            }
        }
        u2 = new double[pRow.length][pCol.length];
        for(int r = 0; r < pRow.length; r++){
            for(int c = 0; c < pCol.length; c++){
                u2[r][c] = U2[r][c];
            }
        }

        //Solve it
        boolean reduced = solveIterativeDomination();
        while (reduced) {
            showNormalFormProblem();
            reduced = solveIterativeDomination();
        }
    }

    public void showNormalFormProblem() {
        System.out.print("****");
        for (int j = 0; j< pCol.length; j++)  if (pCol[j])
            System.out.print("***********");
        System.out.println();
        System.out.print("  ");
        for (int j = 0; j<pCol.length; j++)  if (pCol[j]) {
            if (colMoves.size()>0) {
                System.out.print("      ");
                System.out.print(colMoves.get(j));
                System.out.print("    ");
            }
            else {
                System.out.print("\t");
                System.out.print("Col " +j);
            }
        }
        System.out.println();
        for (int i = 0; i<pRow.length; i++) if (pRow[i]) {
            if (rowMoves.size()>0) System.out.print(rowMoves.get(i)+ ": ");
            else System.out.print("Row " +i+ ": ");
            for (int j = 0; j<pCol.length; j++)  if (pCol[j]) {
                String fs = String.format("| %3.0f,%3.0f", u1[i][j], u2[i][j]);
                System.out.print(fs+"  ");
            }
            System.out.println("|");
        }
        System.out.print("****");
        for (int j = 0; j<pCol.length; j++)  if (pCol[j])
            System.out.print("***********");
        System.out.println();
    }

    public  boolean solveIterativeDomination() throws LPException {
        for (int j = 0; j<pCol.length; j++) if (pCol[j]) {
            if (dominatedColumn(j, u2, pRow, pCol)) {
                System.out.println("The column " +j+ " is dominated!");
                pCol[j]=false;
                return true;
            }
            else System.out.println("The column " +j+ " is not dominated!");
        }
        double[][] t1 = transpose(u1);
        for (int i = 0; i<pRow.length; i++) if (pRow[i]) {
            if (dominatedColumn(i, t1, pCol, pRow)) {
                System.out.println("The row " +i+ " is dominated!");
                pRow[i]=false;
                return true;
            }
            else System.out.println("The row " +i+ " is not dominated!");
        }
        return false;
    }

    public static double[][] transpose(double[][] u) {
        int lin = u.length;
        int col = u[0].length;
        double[][] t = new double[col][lin];
        for (int i = 0; i<lin; i++)
            for (int j = 0; j<col; j++) t[j][i] = u[i][j];
        return t;
    }


    //Strict dominance
    public static boolean dominatedColumn(int jDom, double[][] u, boolean[] pRow, boolean[] pCol) throws LPException {
        for(int c = 0; c < pCol.length; c++){
            if(pCol[c] && c != jDom){
                boolean [] isDom = new boolean[pRow.length];
                for(int r = 0; r < u.length; r++){
                    if(pRow[r]){
                        if(u[r][c] > u[r][jDom]) {
                            isDom[r] = true;
                        }
                    } else {
                        isDom[r] = true;
                    }
                }
                int bCounter = 0;
                for (int b = 0; b < isDom.length; b++){
                    if (isDom[b]) bCounter++;
                }
                if (bCounter == isDom.length){
                    pCol[jDom] = false;
                    return true;
                }
            }
        }
        return false;
    }


    public int unDominatedRows() {
        int counter = 0;
        for (boolean row : pRow){
            if (row) counter++;
        }
        return counter;
    }

    public int unDominatedCols() {
        int counter = 0;
        for (boolean row : pCol){
            if (row) counter++;
        }
        return counter;
    }

    public boolean is2v2Game() {
        return unDominatedCols() == 2 && unDominatedRows() == 2;
    }

    public double[][] getMixedNashEquilibrium() throws LPException {
        //For 2v2
        return getSolutionsFromLP();
    }

    public boolean uniqueNashEquilibrium() {
        return unDominatedCols() == 1 && unDominatedRows() == 1;
    }

    public double[][] getUniqueNashEquilibrium(){
        int row = 0;
        int col = 0;

        for (int i = 0; i < pRow.length; i++){
            if (pRow[i]) row = i;
        }

        for (int i = 0; i < pCol.length; i++){
            if (pCol[i]) col = i;
        }

        double[] movesP1 = new double[pRow.length];
        movesP1[row] = 1.0;

        double[] movesP2 = new double[pCol.length];
        movesP2[col] = 1.0;

        return new double[][]{movesP1, movesP2};
    }

    public boolean isZeroSumGame(){
        for (int i = 0; i < pRow.length; i++){
            for (int j = 0; i < pCol.length; j++){
                if (pRow[i] && pRow[j])
                    if (u1[i][j] + u2[i][j] != 0){
                        return false;
                    }
            }
        }

        return true;
    }

    public double[][] solveZeroSumGame() throws LPException {

        return getSolutionsFromLP();
    }

    private void scale(double[] movesP2) {
        double sum = 0;
        for (int i = 0; i < movesP2.length; i++){
            sum+=movesP2[i];
        }
        for (int i = 0; i < movesP2.length; i++){
            movesP2[i] = movesP2[i]/sum;
        }
    }

    /*****************************************************************/

    public double[][] getSolutionsFromLP() throws LPException {
        double[] movesP2 = {-1};
        double[] movesP1 = {-1};

        LinearProgrammingProblem problem1 = LinearProgramming.setProblem(u1, pCol);
        LinearProgramming.showProblem(problem1);
        LinearProgramming.solveProblem(problem1);
        if (problem1.feasible && problem1.bounded) {
            movesP2 = LinearProgramming.getSolution(problem1);
            scale(movesP2);
        }

        LinearProgrammingProblem problem2 = LinearProgramming.setProblem(transpose(u2), pRow);
        LinearProgramming.showProblem(problem2);
        LinearProgramming.solveProblem(problem2);
        if (problem2.feasible && problem1.bounded){
            movesP1 = LinearProgramming.getSolution(problem2);
            scale(movesP1);
        }

        return new double[][]{movesP1, movesP2};
    }

    public static List<boolean[]> getSubSets(int j, int k, int n) {
        boolean[] b = new boolean[n];
        List<boolean[]> subset = new ArrayList<boolean[]>();
        if (k==0) {
            for(int i=0;i<b.length;i++) b[i]=false;
            subset.add(b);
        }
        else
        if (n-j==k) {
            for(int i=0;i<b.length;i++) b[i]=true;
            subset.add(b);
        }
        else {
            List<boolean[]> s1=getSubSets(j+1,k-1,n);
            for(int i=0;i<s1.size();i++) {
                b = s1.get(i);
                b[j]=true;
                subset.add(b);
            }
            List<boolean[]> s0=getSubSets(j+1,k,n);
            for(int i=0;i<s0.size();i++) {
                b = s0.get(i);
                b[j]=false;
                subset.add(b);
            }
        }
        return subset;
    }

    public static void showSubSet(List<boolean[]> s) {
        int n = s.get(0).length;
        boolean[] b = new boolean[n];
        for(int i=0;i<s.size();i++) {
            b = s.get(i);
            System.out.print("{");
            for(int j=0;j<n;j++)
                if (b[j]) System.out.print(" " + 1);
                else System.out.print(" " + 0);
            System.out.println(" }");
        }
    }

    public static void main(String[] args) throws LPException {
        //List<boolean[]> list = getSubSets(1, 2, 4);
        //showSubSet(list);

        /*String[]labelsP1 = {"Top", "Bottom"};
        String[]labelsP2 = {"Left", "Middle", "Right"};
        int[][] U1 = {{30, -10, 20}, {-10, 20, -20}};
        int[][] U2 = {{-30, 10, -20}, {10, -20, 20}};*/

        String[]labelsP2 = {"Top", "Bottom"};
        String[]labelsP1 = {"Left", "Middle", "Right"};

        int[][] U1 = {{3, 3}, {2, 5}, {0, 6}};
        int[][] U2 = {{3, 2}, {2, 6}, {3, 1}};

        boolean[] supportP1 = {true, false, false};
        boolean[] supportP2 = {true, false};

        NormalFormProblem problem = new NormalFormProblem(labelsP1, labelsP2, U1, U2);
        double[][] moves = problem.solveZeroSumGame();


        System.out.println("Player 1 moves");
        for (int i = 0; i < moves[0].length; i++){
            System.out.print(moves[0][i]+" ");
        }System.out.println();

        System.out.println("Player 2 moves");
        for (int i = 0; i < moves[1].length; i++){
            System.out.print(moves[1][i]+" ");
        }System.out.println();
    }
}
