package play.Tournament2;

import LH.Game;
import LH.Pair;
import cern.colt.matrix.tdouble.DoubleMatrix2D;
import cern.colt.matrix.tdouble.impl.DenseDoubleMatrix2D;
import com.winvector.lp.LPException;
import gametree.GameNode;
import gametree.GameNodeDoesNotExistException;
import lp.NormalFormProblem;
import play.PlayStrategy;
import play.Strategy;
import play.exception.InvalidStrategyException;

import java.util.*;

import static java.lang.Math.max;

/**
 * Created by pedroafonso at 2019-06-08
 */

public class Game6Strategy extends Strategy {
    @Override
    public void execute() throws InterruptedException {
        while (!this.isTreeKnown()) {
            System.err.println("Waiting for game tree to become available.");
            Thread.sleep(1000);
        }
        while (true) {
            PlayStrategy myStrategy = this.getStrategyRequest();
            if (myStrategy == null) //Game was terminated by an outside event
                break;
            boolean playComplete = false;

            while (!playComplete) {
                System.out.println("*******************************************************");
                if (myStrategy.getFinalP1Node() != -1) {
                    GameNode finalP1 = this.tree.getNodeByIndex(myStrategy.getFinalP1Node());
                    GameNode fatherP1 = null;
                    if (finalP1 != null) {
                        try {
                            fatherP1 = finalP1.getAncestor();
                        } catch (GameNodeDoesNotExistException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.print("Last round as P1: " + showLabel(fatherP1.getLabel()) + "|" + showLabel(finalP1.getLabel()));
                        System.out.println(" -> (Me) " + finalP1.getPayoffP1() + " : (Opp) " + finalP1.getPayoffP2());
                    }
                }
                if (myStrategy.getFinalP2Node() != -1) {
                    GameNode finalP2 = this.tree.getNodeByIndex(myStrategy.getFinalP2Node());
                    GameNode fatherP2 = null;
                    if (finalP2 != null) {
                        try {
                            fatherP2 = finalP2.getAncestor();
                        } catch (GameNodeDoesNotExistException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.print("Last round as P2: " + showLabel(fatherP2.getLabel()) + "|" + showLabel(finalP2.getLabel()));
                        System.out.println(" -> (Opp) " + finalP2.getPayoffP1() + " : (Me) " + finalP2.getPayoffP2());
                    }
                }
                GameNode rootNode = tree.getRootNode();
                int n1 = rootNode.numberOfChildren();
                int n2 = rootNode.getChildren().next().numberOfChildren();
                String[] labelsP1 = new String[n1];
                String[] labelsP2 = new String[n2];
                int[][] U1 = new int[n1][n2];
                int[][] U2 = new int[n1][n2];
                Iterator<GameNode> childrenNodes1 = rootNode.getChildren();
                GameNode childNode1;
                GameNode childNode2;
                int i = 0;
                int j = 0;
                while (childrenNodes1.hasNext()) {
                    childNode1 = childrenNodes1.next();
                    labelsP1[i] = childNode1.getLabel();
                    j = 0;
                    Iterator<GameNode> childrenNodes2 = childNode1.getChildren();
                    while (childrenNodes2.hasNext()) {
                        childNode2 = childrenNodes2.next();
                        if (i == 0) labelsP2[j] = childNode2.getLabel();
                        U1[i][j] = childNode2.getPayoffP1();
                        U2[i][j] = childNode2.getPayoffP2();
                        j++;
                    }
                    i++;
                }
                showMoves(1, labelsP1);
                showMoves(2, labelsP2);
                showUtility(1, n1, n2, U1);
                showUtility(2, n1, n2, U2);
                try {
                    setStrategyWithoutDominated(labelsP1, labelsP2, U1, U2, myStrategy);
                } catch (LPException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                //showStrategy(1,strategyP1,labelsP1);
                //showStrategy(2,strategyP2,labelsP2);
                try {
                    this.provideStrategy(myStrategy);
                    playComplete = true;
                } catch (InvalidStrategyException e) {
                    System.err.println("Invalid strategy: " + e.getMessage());
                    ;
                    e.printStackTrace(System.err);
                }
            }
        }

    }

    public String showLabel(String label) {
        return label.substring(label.lastIndexOf(':') + 1);
    }

    public void showMoves(int P, String[] labels) {
        System.out.println("Moves Player " + P + ":");
        for (int i = 0; i < labels.length; i++) System.out.println("   " + showLabel(labels[i]));
    }

    public void showUtility(int P, int n1, int n2, int[][] M) {
        System.out.println("Utility Player " + P + ":");
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n2; j++) System.out.print("| " + M[i][j] + " ");
            System.out.println("|");
        }
    }


    public void setStrategyWithoutDominated(String[] labelsP1, String[] labelsP2, int[][] U1, int[][] U2, PlayStrategy myStrategy) throws LPException {


        System.out.println("APPLYING LH TO GEN-SUM GAME");
        double[][] U1aux = new double[U1.length][U1[0].length];
        double[][] U2aux = new double[U2.length][U2[0].length];

        for (int i = 0; i < U1aux.length; i++) {
            for (int j = 0; j < U1aux[0].length; j++) {
                U1aux[i][j] = U1[i][j];
                U2aux[i][j] = U2[i][j];
            }
        }

        DoubleMatrix2D A = new DenseDoubleMatrix2D(U1aux);
        DoubleMatrix2D B = new DenseDoubleMatrix2D(U2aux);

        Game game = new Game(A, B);
        Set<Pair> result = game.enumerateLemkeHowson();

        //Iterate through results and choose the NE with the greatest nr
        //of actions in the support
        Pair moves = null;
        int actionsInTheSupport = -1;
        for (Pair p : result) {
            List<Double> movesP1 = p.getA();
            List<Double> movesP2 = p.getB();

            int auxSupport = actionsInTheSupport(movesP1) + actionsInTheSupport(movesP2);
            if (auxSupport > actionsInTheSupport){
                actionsInTheSupport = auxSupport;
                moves = p;
            }
        }

        double[] movesP1;
        double[] movesP2;

        movesP1 = new double[moves.getA().size()];
        for (int i = 0; i < moves.getA().size(); i++) movesP1[i] = moves.getA().get(i);

        movesP2 = new double[moves.getB().size()];
        for (int i = 0; i < moves.getB().size(); i++) movesP2[i] = moves.getB().get(i);

        showStrategy(1, movesP1, labelsP1);
        showStrategy(2, movesP2, labelsP2);

        //Playing for P1
        for (int i = 0; i < labelsP1.length; i++) {
            myStrategy.put(labelsP1[i], movesP1[i]);
        }

        //Playing for P2
        for (int i = 0; i < labelsP2.length; i++) {
            myStrategy.put(labelsP2[i], movesP2[i]);
        }
    }

    public void showStrategy(int P, double[] strategy, String[] labels) {
        System.out.println("Strategy Player " + P + ":");
        for (int i = 0; i < labels.length; i++) System.out.println("   " + strategy[i] + ":" + showLabel(labels[i]));
    }

    public int actionsInTheSupport(List<Double> moves){
        int counter = 0;
        for (double m : moves){
            if (m != 0.0) counter++;
        }

        return counter;
    }

}
