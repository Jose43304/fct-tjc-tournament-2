package play.Tournament2;

import com.winvector.lp.LPException;
import gametree.GameNode;
import gametree.GameNodeDoesNotExistException;
import lp.NormalFormProblem;
import play.PlayStrategy;
import play.Strategy;
import play.exception.InvalidStrategyException;

import java.util.*;

import static java.lang.Math.max;

/**
 * Created by pedroafonso at 2019-06-08
 */

public class Game7Strategy extends Strategy {

    private int maxminInARow = 0;
    private int maxminInARowP1 = 0;
    private int maxminInARowP2 = 0;
    private int round = 0;

    private int myP1LastMove = -1;
    private int myP2LastMove = -1;

    @Override
    public void execute() throws InterruptedException {
        while (!this.isTreeKnown()) {
            System.err.println("Waiting for game tree to become available.");
            Thread.sleep(1000);
        }
        while (true) {
            PlayStrategy myStrategy = this.getStrategyRequest();
            if (myStrategy == null) //Game was terminated by an outside event
                break;
            boolean playComplete = false;

            while (!playComplete) {
                System.out.println("*******************************************************");
                if (myStrategy.getFinalP1Node() != -1) {
                    GameNode finalP1 = this.tree.getNodeByIndex(myStrategy.getFinalP1Node());
                    GameNode fatherP1 = null;
                    if (finalP1 != null) {
                        try {
                            fatherP1 = finalP1.getAncestor();
                        } catch (GameNodeDoesNotExistException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.print("Last round as P1: " + showLabel(fatherP1.getLabel()) + "|" + showLabel(finalP1.getLabel()));
                        System.out.println(" -> (Me) " + finalP1.getPayoffP1() + " : (Opp) " + finalP1.getPayoffP2());
                    }
                }
                if (myStrategy.getFinalP2Node() != -1) {
                    GameNode finalP2 = this.tree.getNodeByIndex(myStrategy.getFinalP2Node());
                    GameNode fatherP2 = null;
                    if (finalP2 != null) {
                        try {
                            fatherP2 = finalP2.getAncestor();
                        } catch (GameNodeDoesNotExistException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.print("Last round as P2: " + showLabel(fatherP2.getLabel()) + "|" + showLabel(finalP2.getLabel()));
                        System.out.println(" -> (Opp) " + finalP2.getPayoffP1() + " : (Me) " + finalP2.getPayoffP2());
                    }
                }
                GameNode rootNode = tree.getRootNode();
                int n1 = rootNode.numberOfChildren();
                int n2 = rootNode.getChildren().next().numberOfChildren();
                String[] labelsP1 = new String[n1];
                String[] labelsP2 = new String[n2];
                int[][] U1 = new int[n1][n2];
                int[][] U2 = new int[n1][n2];
                Iterator<GameNode> childrenNodes1 = rootNode.getChildren();
                GameNode childNode1;
                GameNode childNode2;
                int i = 0;
                int j = 0;
                while (childrenNodes1.hasNext()) {
                    childNode1 = childrenNodes1.next();
                    labelsP1[i] = childNode1.getLabel();
                    j = 0;
                    Iterator<GameNode> childrenNodes2 = childNode1.getChildren();
                    while (childrenNodes2.hasNext()) {
                        childNode2 = childrenNodes2.next();
                        if (i == 0) labelsP2[j] = childNode2.getLabel();
                        U1[i][j] = childNode2.getPayoffP1();
                        U2[i][j] = childNode2.getPayoffP2();
                        j++;
                    }
                    i++;
                }
                showMoves(1, labelsP1);
                showMoves(2, labelsP2);
                showUtility(1, n1, n2, U1);
                showUtility(2, n1, n2, U2);
                try {
                    setStrategyWithoutDominated(labelsP1, labelsP2, U1, U2, myStrategy);
                } catch (LPException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                //showStrategy(1,strategyP1,labelsP1);
                //showStrategy(2,strategyP2,labelsP2);
                try {
                    this.provideStrategy(myStrategy);
                    playComplete = true;
                } catch (InvalidStrategyException e) {
                    System.err.println("Invalid strategy: " + e.getMessage());
                    ;
                    e.printStackTrace(System.err);
                }
            }
        }

    }

    public String showLabel(String label) {
        return label.substring(label.lastIndexOf(':') + 1);
    }

    public void showMoves(int P, String[] labels) {
        System.out.println("Moves Player " + P + ":");
        for (int i = 0; i < labels.length; i++) System.out.println("   " + showLabel(labels[i]));
    }

    public void showUtility(int P, int n1, int n2, int[][] M) {
        System.out.println("Utility Player " + P + ":");
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n2; j++) System.out.print("| " + M[i][j] + " ");
            System.out.println("|");
        }
    }


    //TODO: test this
    public void setStrategyWithoutDominated(String[] labelsP1, String[] labelsP2, int[][] U1, int[][] U2, PlayStrategy myStrategy) throws LPException {
        double[] movesP1;
        double[] movesP2;
        NormalFormProblem problem = new NormalFormProblem(labelsP1, labelsP2, U1, U2);

        if (round < 4) {
            //"Bone" Rounds
            System.out.println("BONE ACTIONS");
            //Play the first row/col that isn't dominated

            movesP1 = new double[labelsP1.length];
            Arrays.fill(movesP1, 0.0);
            int p1Move = -1;
            for (int i = 0; i < problem.pRow.length; i++){
                if (problem.pRow[i]){
                    movesP1[i] = 1.0;
                    p1Move = i;
                    break;
                }
            }

            movesP2 = new double[labelsP2.length];
            Arrays.fill(movesP2, 0.0);
            int p2Move = -1;
            for (int i = 0; i < problem.pCol.length; i++){
                if (problem.pCol[i]){
                    movesP2[i] = 1.0;
                    p2Move = i;
                    break;
                }
            }

            int oppMove = oppHasPlayedMaxMin(myStrategy, p1Move, p2Move, problem);

            if (oppMove == 3){
                //Both players are playing maxmin
                maxminInARow++;
                maxminInARowP1++;
                maxminInARowP2++;
            } else if (oppMove == 1){
                //Player 1 is playing maxmin
                maxminInARowP1++;
            } else if (oppMove == 2){
                //Player 2 is playing maxmin
                maxminInARowP2++;
            } else
                maxminInARow = 0;

        } else {
            if (maxminInARow == 3 && oppP1StillMaxMin(problem, myStrategy) && oppP2StillMaxMin(problem, myStrategy)) {
                //Play the BR against both players with maxmin
                System.out.println("BOTH PLAYERS MAXMIN");
                movesP1 = bestResponseAgainstMaxminP2(U1, getP2MaxMinMove(problem));
                movesP2 = bestResponseAgainstMaxminP1(U2, getP1MaxMinMove(problem));

            } else if (maxminInARowP1 == 3 && oppP1StillMaxMin(problem, myStrategy)) {
                //Play the BR against player 2 with maxmin
                System.out.println("PLAYER 2 MAXMIN");
                movesP1 = problem.solveZeroSumGame()[0];
                movesP2 = bestResponseAgainstMaxminP1(U2, getP1MaxMinMove(problem));

            } else if (maxminInARowP2 == 3 && oppP2StillMaxMin(problem, myStrategy)) {
                //Play the BR against player 1 with maxmin
                System.out.println("PLAYER 1 MAXMIN");
                movesP2 = problem.solveZeroSumGame()[1];
                movesP1 = bestResponseAgainstMaxminP2(U1, getP2MaxMinMove(problem));

            } else {
                double[][] solution;

                System.out.println("ZERO SUM GAME");
                solution = problem.solveZeroSumGame();

                movesP1 = solution[0];
                movesP2 = solution[1];
            }
            for (int i = 0; i < movesP1.length; i++){
                if (movesP1[i] == 1.0){
                    myP1LastMove = i;
                    break;
                }
            }

            for (int i = 0; i < movesP2.length; i++){
                if (movesP2[i] == 1.0){
                    myP2LastMove = i;
                    break;
                }
            }
        }
        round++;

        showStrategy(1, movesP1, labelsP1);
        showStrategy(2, movesP2, labelsP2);

        //Playing for P1
        for (int i = 0; i < labelsP1.length; i++) {
            myStrategy.put(labelsP1[i], movesP1[i]);
        }

        //Playing for P2
        for (int i = 0; i < labelsP2.length; i++) {
            myStrategy.put(labelsP2[i], movesP2[i]);
        }

        System.out.println("maxminInaRow: "+maxminInARow);
        System.out.println("maxminInaRowP1: "+maxminInARowP1);
        System.out.println("maxminInaRowP2: "+maxminInARowP2);
        System.out.println("round: "+round);
    }

    private boolean oppP1StillMaxMin(NormalFormProblem problem, PlayStrategy myStrategy) {
        if (myP1LastMove == -1) return true;
        else
            if (myStrategy.getLastRoundSelfScoreAsP1(this.tree) ==
                    problem.u1[myP1LastMove][getP2MaxMinMove(problem)]) {
                return true;
            } else {
                return false;
            }
    }

    private boolean oppP2StillMaxMin(NormalFormProblem problem, PlayStrategy myStrategy) {
        if (myP2LastMove == -1) return true;
        else
        if (myStrategy.getLastRoundSelfScoreAsP2(this.tree) ==
                problem.u2[getP1MaxMinMove(problem)][myP2LastMove]) {
            return true;
        } else {
            return false;
        }

    }

    private double[] bestResponseAgainstMaxminP2(int[][] u1, int p2MaxMinMove) {
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < u1.length; i++){
            if (u1[i][p2MaxMinMove] > max){
                max = u1[i][p2MaxMinMove];
                maxIndex = i;
            }
        }
        double[] moves = new double[u1.length];
        Arrays.fill(moves, 0.0);
        moves[maxIndex] = 1.0;

        return moves;
    }

    private double[] bestResponseAgainstMaxminP1(int[][] u2, int p1MaxMinMove) {
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < u2[0].length; i++){
            if (u2[p1MaxMinMove][i] > max){
                max = u2[p1MaxMinMove][i];
                maxIndex = i;
            }
        }
        double[] moves = new double[u2[0].length];
        Arrays.fill(moves, 0.0);
        moves[maxIndex] = 1.0;

        return moves;
    }

    private int oppHasPlayedMaxMin(PlayStrategy myStrategy, int moveP1, int moveP2, NormalFormProblem problem) {
        if (round == 0 ) {
            System.out.println("FIRST PLAY");
            return -1;
        } else {
            int maxminIndex = getP1MaxMinMove(problem);
            int maxminIndexP2 = getP2MaxMinMove(problem);

            //Find if P1 and P2 have played the maxmin from the reduced game

            System.out.println("maxminIndex: "+maxminIndex);
            System.out.println("moveP2: "+moveP2);
            System.out.println("maxminIndexP2: "+maxminIndexP2);
            System.out.println("moveP1: "+moveP1);
            System.out.println("OppScoreAsP1: "+myStrategy.getLastRoundOpponentScoreAsP1(this.tree));
            System.out.println("OppScoreAsP2: "+myStrategy.getLastRoundOpponentScoreAsP2(this.tree));
            System.out.println("U1: "+problem.u1[maxminIndex][moveP2]);
            System.out.println("U2: "+problem.u2[moveP1][maxminIndexP2]);
            if (problem.u2[moveP1][maxminIndexP2] == myStrategy.getLastRoundOpponentScoreAsP1(this.tree)){
                //P1 is playing
                if (problem.u1[maxminIndex][moveP2] == myStrategy.getLastRoundOpponentScoreAsP2(this.tree)){
                    //P2 is playing
                    return 3;
                } else {
                    return 1;
                }
            } else if (problem.u1[maxminIndex][moveP2] == myStrategy.getLastRoundOpponentScoreAsP2(this.tree)){
                return 2;
            } else {
                return -1;
            }
        }
    }

    private int getP2MaxMinMove(NormalFormProblem problem) {
        double[] minsP2 = new double[problem.pCol.length];
        Arrays.fill(minsP2, Double.NEGATIVE_INFINITY);
        for (int i = 0; i < problem.pCol.length; i++){
            if (problem.pCol[i])
                for (int j = 0; j < problem.pRow.length; j++){
                    if (problem.pRow[j]){
                        if (minsP2[i] == Double.NEGATIVE_INFINITY){
                            minsP2[i] = problem.u2[j][i];
                        } else if (problem.u2[j][i] < minsP2[i]){
                            minsP2[i] = problem.u2[j][i];
                        }
                    }
                }
        }
        double maxminAuxP2 = Double.NEGATIVE_INFINITY;
        int maxminIndexP2 = -1;
        for (int i = 0; i < minsP2.length; i++){
            if (minsP2[i] > maxminAuxP2){
                maxminIndexP2 = i;
                maxminAuxP2 = minsP2[i];
            }
        }

        return maxminIndexP2;
    }

    public void showStrategy(int P, double[] strategy, String[] labels) {
        System.out.println("Strategy Player " + P + ":");
        for (int i = 0; i < labels.length; i++) System.out.println("   " + strategy[i] + ":" + showLabel(labels[i]));
    }

    public int getP1MaxMinMove(NormalFormProblem problem){
        double[] mins = new double[problem.pRow.length];
        Arrays.fill(mins, Double.NEGATIVE_INFINITY);
        for (int i = 0; i < problem.pRow.length; i++){
            if (problem.pRow[i])
                for (int j = 0; j < problem.pCol.length; j++){
                    if (problem.pCol[j]){
                        if (mins[i] == Double.NEGATIVE_INFINITY){
                            mins[i] = problem.u1[i][j];
                        } else if (problem.u1[i][j] < mins[i]){
                            mins[i] = problem.u1[i][j];
                        }
                    }
                }
        }
        for (int i = 0; i < mins.length; i++)
            System.out.println("mins: "+mins[i]);

        double maxminAux = Double.NEGATIVE_INFINITY;
        int maxminIndex = -1;
        for (int i = 0; i < mins.length; i++){
            if (mins[i] > maxminAux){
                maxminIndex = i;
                maxminAux = mins[i];
            }
        }

        return maxminIndex;
    }

}
