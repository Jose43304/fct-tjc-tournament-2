package play.Tournament2;

import LH.Game;
import LH.Pair;
import cern.colt.matrix.tdouble.DoubleMatrix2D;
import cern.colt.matrix.tdouble.impl.DenseDoubleMatrix2D;
import com.winvector.lp.LPException;
import gametree.GameNode;
import gametree.GameNodeDoesNotExistException;
import lp.NormalFormProblem;
import play.PlayStrategy;
import play.Strategy;
import play.exception.InvalidStrategyException;

import java.util.*;

import static java.lang.Math.max;

/**
 * Created by pedroafonso at 2019-06-08
 */

public class Game8Strategy extends Strategy {

    private int maxminInARow = 0;
    private int maxminInARowP1 = 0;
    private int maxminInARowP2 = 0;
    private int round = 0;

    private int myP1LastMove = -1;
    private int myP2LastMove = -1;

    private int p1OppLastScore = Integer.MIN_VALUE;
    private int p2OppLastScore = Integer.MIN_VALUE;

    private int p10OppRepeatedAction = 0;
    private int p20OppRepeatedAction = 0;


    @Override
    public void execute() throws InterruptedException {
        while (!this.isTreeKnown()) {
            System.err.println("Waiting for game tree to become available.");
            Thread.sleep(1000);
        }
        while (true) {
            PlayStrategy myStrategy = this.getStrategyRequest();
            if (myStrategy == null) //Game was terminated by an outside event
                break;
            boolean playComplete = false;

            while (!playComplete) {
                System.out.println("*******************************************************");
                if (myStrategy.getFinalP1Node() != -1) {
                    GameNode finalP1 = this.tree.getNodeByIndex(myStrategy.getFinalP1Node());
                    GameNode fatherP1 = null;
                    if (finalP1 != null) {
                        try {
                            fatherP1 = finalP1.getAncestor();
                        } catch (GameNodeDoesNotExistException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.print("Last round as P1: " + showLabel(fatherP1.getLabel()) + "|" + showLabel(finalP1.getLabel()));
                        System.out.println(" -> (Me) " + finalP1.getPayoffP1() + " : (Opp) " + finalP1.getPayoffP2());
                    }
                }
                if (myStrategy.getFinalP2Node() != -1) {
                    GameNode finalP2 = this.tree.getNodeByIndex(myStrategy.getFinalP2Node());
                    GameNode fatherP2 = null;
                    if (finalP2 != null) {
                        try {
                            fatherP2 = finalP2.getAncestor();
                        } catch (GameNodeDoesNotExistException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.print("Last round as P2: " + showLabel(fatherP2.getLabel()) + "|" + showLabel(finalP2.getLabel()));
                        System.out.println(" -> (Opp) " + finalP2.getPayoffP1() + " : (Me) " + finalP2.getPayoffP2());
                    }
                }
                GameNode rootNode = tree.getRootNode();
                int n1 = rootNode.numberOfChildren();
                int n2 = rootNode.getChildren().next().numberOfChildren();

                String[] labelsP1 = new String[n1];
                String[] labelsP2 = new String[n2];
                int[][] U1 = new int[n1][n2];
                int[][] U2 = new int[n1][n2];
                Iterator<GameNode> childrenNodes1 = rootNode.getChildren();
                GameNode childNode1;
                GameNode childNode2;
                int i = 0;
                int j = 0;
                while (childrenNodes1.hasNext()) {
                    childNode1 = childrenNodes1.next();
                    labelsP1[i] = childNode1.getLabel();
                    System.out.println("LabelP1: "+labelsP1[i]);
                    j = 0;
                    Iterator<GameNode> childrenNodes2 = childNode1.getChildren();
                    while (childrenNodes2.hasNext()) {
                        childNode2 = childrenNodes2.next();
                        if (i == 0) {
                            labelsP2[j] = childNode2.getLabel();
                            System.out.println("LabelP2: "+labelsP2[j]);
                        }
                        U1[i][j] = childNode2.getPayoffP1();
                        U2[i][j] = childNode2.getPayoffP2();
                        j++;
                    }
                    i++;
                }
                showMoves(1, labelsP1);
                showMoves(2, labelsP2);
                showUtility(1, n1, n2, U1);
                showUtility(2, n1, n2, U2);
                try {
                    setStrategyWithoutDominated(labelsP1, labelsP2, U1, U2, myStrategy);
                } catch (LPException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                //showStrategy(1,strategyP1,labelsP1);
                //showStrategy(2,strategyP2,labelsP2);
                try {
                    this.provideStrategy(myStrategy);
                    playComplete = true;
                } catch (InvalidStrategyException e) {
                    System.err.println("Invalid strategy: " + e.getMessage());
                    ;
                    e.printStackTrace(System.err);
                }
            }
        }

    }

    public String showLabel(String label) {
        return label.substring(label.lastIndexOf(':') + 1);
    }

    public void showMoves(int P, String[] labels) {
        System.out.println("Moves Player " + P + ":");
        for (int i = 0; i < labels.length; i++) System.out.println("   " + showLabel(labels[i]));
    }

    public void showUtility(int P, int n1, int n2, int[][] M) {
        System.out.println("Utility Player " + P + ":");
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n2; j++) System.out.print("| " + M[i][j] + " ");
            System.out.println("|");
        }
    }


    public void setStrategyWithoutDominated(String[] labelsP1, String[] labelsP2, int[][] U1, int[][] U2, PlayStrategy myStrategy) throws LPException {
        NormalFormProblem problem = new NormalFormProblem(labelsP1, labelsP2, U1, U2);
        double[][] solution;

        double[] movesP1;
        double[] movesP2;

        solution = findMaxSocialWelfare(U1, U2);
        movesP1 = solution[0];
        movesP2 = solution[1];

        if (!opponentIsCoop(myStrategy)){
            System.out.println("teste1");
            if (hasOppP1PlayedTheSameAction(myStrategy)) {
                p10OppRepeatedAction++;
                System.out.println("teste2");
            } else {
                System.out.println("teste3");
                p10OppRepeatedAction = 0;
            }

            if (hasOppP2PlayedTheSameAction(myStrategy)) {
                System.out.println("teste4");
                p20OppRepeatedAction++;
            } else {
                System.out.println("teste5");
                p20OppRepeatedAction = 0;
            }

            if (p10OppRepeatedAction > 3 && p20OppRepeatedAction > 3){
                System.out.println("BOTH REPEATED ACTION");
                movesP1 = BRToRepeatedActionsFromP1(U1, U2, myStrategy);
                movesP2 = BRToRepeatedActionsFromP2(U1, U2, myStrategy);

            } else if (p10OppRepeatedAction > 3){
                System.out.println("1 REPEATED ACTION");
                movesP1 = BRToRepeatedActionsFromP1(U1, U2, myStrategy);
            } else if (p20OppRepeatedAction > 3){
                System.out.println("2 REPEATED ACTION");
                movesP2 = BRToRepeatedActionsFromP2(U1, U2, myStrategy);
            } else {
                if (problem.uniqueNashEquilibrium()) {
                    System.out.println("UNIQUE NASH");
                    solution = problem.getUniqueNashEquilibrium();
                    movesP1 = solution[0];
                    movesP2 = solution[1];

                } else if (problem.is2v2Game()) {
                    System.out.println("2 V 2 MIXED NASH");

                    solution = problem.getMixedNashEquilibrium();
                    movesP1 = solution[0];
                    movesP2 = solution[1];

                } else if (problem.isZeroSumGame()) {
                    System.out.println("ZERO SUM GAME");
                    solution = problem.solveZeroSumGame();
                    movesP1 = solution[0];
                    movesP2 = solution[1];

                } else {
                    System.out.println("APPLYING LH TO GEN-SUM GAME");
                    double[][] U1aux = new double[U1.length][U1[0].length];
                    double[][] U2aux = new double[U2.length][U2[0].length];

                    for (int i = 0; i < U1aux.length; i++) {
                        for (int j = 0; j < U1aux[0].length; j++) {
                            U1aux[i][j] = U1[i][j];
                            U2aux[i][j] = U2[i][j];
                        }
                    }

                    DoubleMatrix2D A = new DenseDoubleMatrix2D(U1aux);
                    DoubleMatrix2D B = new DenseDoubleMatrix2D(U2aux);


                    Game game = new Game(A, B);
                    Set<Pair> result = game.enumerateLemkeHowson();

                    //Iterate through results and choose the one that maximizes
                    // my welfare
                    double maxSumP1 = Double.MIN_VALUE;
                    double maxSumP2 = Double.MIN_VALUE;
                    List<Double> movesP1list = null;
                    List<Double> movesP2list = null;
                    for (Pair p : result) {
                        List<Double> movesP2aux = p.getB();
                        List<Double> movesP1aux = p.getA();

                        double sumUtilityP1 = 0.0;
                        for (int i = 0; i < U1.length; i++) {
                            for (int j = 0; j < U1[0].length; j++) {
                                sumUtilityP1 += movesP1aux.get(i) * U1[i][j];
                            }
                        }

                        if (sumUtilityP1 > maxSumP1) {
                            maxSumP1 = sumUtilityP1;
                            movesP1list = movesP1aux;
                        }

                        double sumUtilityP2 = 0.0;
                        for (int i = 0; i < U2[0].length; i++) {
                            for (int j = 0; j < U2.length; j++) {
                                sumUtilityP2 += movesP2aux.get(i) * U2[j][i];
                            }
                        }

                        if (sumUtilityP2 > maxSumP2) {
                            maxSumP2 = sumUtilityP2;
                            movesP2list = movesP2aux;
                        }
                    }

                    double[] movesP1Aux = new double[movesP1list.size()];
                    for (int i = 0; i < movesP1list.size(); i++) movesP1Aux[i] = movesP1list.get(i);

                    double[] movesP2Aux = new double[movesP2list.size()];
                    for (int i = 0; i < movesP2list.size(); i++) movesP2Aux[i] = movesP2list.get(i);

                    solution = new double[][]{movesP1Aux, movesP2Aux};
                    movesP1 = solution[0];
                    movesP2 = solution[1];

                }
            }
        }

        round++;

        showStrategy(1, movesP1, labelsP1);
        showStrategy(2, movesP2, labelsP2);

        System.out.println("p1OppLastScore: "+p1OppLastScore);

        p1OppLastScore = myStrategy.getLastRoundOpponentScoreAsP1(this.tree);
        p2OppLastScore = myStrategy.getLastRoundOpponentScoreAsP2(this.tree);

        System.out.println("P1Counter: "+p10OppRepeatedAction);
        System.out.println("P2Counter: "+p20OppRepeatedAction);
        System.out.println("p1OppLastScore: "+p1OppLastScore);


        //Playing for P1
        for(int i = 0; i < labelsP1.length; i++) {
            myStrategy.put(labelsP1[i], movesP1[i]);
        }

        //Playing for P2
        for(int i = 0; i < labelsP2.length; i++) {
            myStrategy.put(labelsP2[i], movesP2[i]);
        }

    }

    private double[] BRToRepeatedActionsFromP2(int[][] u1, int[][] u2, PlayStrategy myStrategy) {
        int oppPayoff = myStrategy.getLastRoundOpponentScoreAsP2(this.tree);
        int myPayoff = myStrategy.getLastRoundSelfScoreAsP2(this.tree);

        int action = -1;

        for (int i = 0; i < u1.length; i++){
            for (int j = 0; j < u1[0].length; j++){
                if (u1[i][j] == myPayoff && u2[i][j] == oppPayoff){
                    action = i;
                    break;
                }
            }
        }

        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < u2[0].length; i++){
            if (u2[action][i] > max){
                max = u2[action][i];
                maxIndex = i;
            }
        }

        double moves[] = new double[u2[0].length];
        Arrays.fill(moves, 0.0);
        moves[maxIndex] = 1.0;

        return moves;
    }

    private double[] BRToRepeatedActionsFromP1(int[][] u1, int[][] u2, PlayStrategy myStrategy) {
        int oppPayoff = myStrategy.getLastRoundOpponentScoreAsP1(this.tree);
        int myPayoff = myStrategy.getLastRoundSelfScoreAsP1(this.tree);

        int action = -1;

        for (int i = 0; i < u1.length; i++){
            for (int j = 0; j < u1[0].length; j++){
                if (u1[i][j] == myPayoff && u2[i][j] == oppPayoff){
                    action = j;
                    break;
                }
            }
        }

        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < u1.length; i++){
            if (u1[i][action] > max){
                max = u1[i][action];
                maxIndex = i;
            }
        }

        double moves[] = new double[u1.length];
        Arrays.fill(moves, 0.0);
        moves[maxIndex] = 1.0;

        return moves;
    }

    private boolean opponentIsCoop(PlayStrategy myStrategy) {
        if (round == 0) {
            return true;
        } else {
            if (myStrategy.getLastRoundSelfScoreAsP1(this.tree) + myStrategy.getLastRoundSelfScoreAsP2(this.tree) ==
                    myStrategy.getLastRoundOpponentScoreAsP2(this.tree) + myStrategy.getLastRoundOpponentScoreAsP1(this.tree)
            )
                return true;
        }
        return false;
    }

    public void showStrategy(int P, double[] strategy, String[] labels) {
        System.out.println("Strategy Player " + P + ":");
        for (int i = 0; i < labels.length; i++) System.out.println("   " + strategy[i] + ":" + showLabel(labels[i]));
    }

    public double[][] findMaxSocialWelfare(int[][] u1, int [][] u2){
        int maxSocialWelfare = Integer.MIN_VALUE;
        int u1Index = -1;
        int u2Index = -2;

        for (int i = 0; i < u1.length; i++){
            for (int j = 0; j < u1[0].length; j++){
                if (u1[i][j] + u2[i][j] > maxSocialWelfare){
                    maxSocialWelfare = u1[i][j] + u2[i][j];
                    u1Index = i;
                    u2Index = j;
                }
            }
        }

        double[] movesP1 = new double[u1.length];
        Arrays.fill(movesP1, 0.0);
        movesP1[u1Index] = 1.0;

        double[] movesP2 = new double[u1[0].length];
        Arrays.fill(movesP2, 0.0);
        movesP2[u2Index] = 1.0;

        return new double[][]{movesP1, movesP2};
    }

    public boolean hasOppP1PlayedTheSameAction(PlayStrategy myStrategy){
        if (p1OppLastScore == Integer.MIN_VALUE)
            return false;
        else if (p1OppLastScore == myStrategy.getLastRoundOpponentScoreAsP1(this.tree)){
            return true;
        }
        return false;
    }

    public boolean hasOppP2PlayedTheSameAction(PlayStrategy myStrategy){
        if (p2OppLastScore == Integer.MIN_VALUE)
            return false;
        else if (p2OppLastScore == myStrategy.getLastRoundOpponentScoreAsP2(this.tree)){
            return true;
        }
        return false;
    }
}

