package play.Tournament2;

import com.winvector.lp.LPException;
import gametree.GameNode;
import gametree.GameNodeDoesNotExistException;
import lp.NormalFormProblem;
import play.PlayStrategy;
import play.Strategy;
import play.exception.InvalidStrategyException;

import java.util.*;

import static java.lang.Math.min;

/**
 * Created by pedroafonso at 2019-06-08
 */

public class Game4Strategy extends Strategy {
    @Override
    public void execute() throws InterruptedException {
        while(!this.isTreeKnown()) {
            System.err.println("Waiting for game tree to become available.");
            Thread.sleep(1000);
        }
        while(true) {
            PlayStrategy myStrategy = this.getStrategyRequest();
            if(myStrategy == null) //Game was terminated by an outside event
                break;
            boolean playComplete = false;

            while(! playComplete ) {
                System.out.println("*******************************************************");
                if(myStrategy.getFinalP1Node() != -1) {
                    GameNode finalP1 = this.tree.getNodeByIndex(myStrategy.getFinalP1Node());
                    GameNode fatherP1 = null;
                    if(finalP1 != null) {
                        try {
                            fatherP1 = finalP1.getAncestor();
                        } catch (GameNodeDoesNotExistException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.print("Last round as P1: " + showLabel(fatherP1.getLabel()) + "|" + showLabel(finalP1.getLabel()));
                        System.out.println(" -> (Me) " + finalP1.getPayoffP1() + " : (Opp) "+ finalP1.getPayoffP2());
                    }
                }
                if(myStrategy.getFinalP2Node() != -1) {
                    GameNode finalP2 = this.tree.getNodeByIndex(myStrategy.getFinalP2Node());
                    GameNode fatherP2 = null;
                    if(finalP2 != null) {
                        try {
                            fatherP2 = finalP2.getAncestor();
                        } catch (GameNodeDoesNotExistException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        System.out.print("Last round as P2: " + showLabel(fatherP2.getLabel()) + "|" + showLabel(finalP2.getLabel()));
                        System.out.println(" -> (Opp) " + finalP2.getPayoffP1() + " : (Me) "+ finalP2.getPayoffP2());
                    }
                }
                GameNode rootNode = tree.getRootNode();
                int n1 = rootNode.numberOfChildren();
                int n2=rootNode.getChildren().next().numberOfChildren();
                String[] labelsP1 = new String[n1];
                String[] labelsP2 = new String[n2];
                int[][] U1 = new int[n1][n2];
                int[][] U2 = new int[n1][n2];
                Iterator<GameNode> childrenNodes1 = rootNode.getChildren();
                GameNode childNode1;
                GameNode childNode2;
                int i = 0;
                int j = 0;
                while(childrenNodes1.hasNext()) {
                    childNode1 = childrenNodes1.next();
                    labelsP1[i] = childNode1.getLabel();
                    j = 0;
                    Iterator<GameNode> childrenNodes2 = childNode1.getChildren();
                    while(childrenNodes2.hasNext()) {
                        childNode2 = childrenNodes2.next();
                        if (i==0) labelsP2[j] = childNode2.getLabel();
                        U1[i][j] = childNode2.getPayoffP1();
                        U2[i][j] = childNode2.getPayoffP2();
                        j++;
                    }
                    i++;
                }
                showMoves(1,labelsP1);
                showMoves(2,labelsP2);
                showUtility(1,n1,n2,U1);
                showUtility(2,n1,n2,U2);
                try {
                    setStrategyAgainstMaxMin(labelsP1, labelsP2, U1, U2, myStrategy);
                } catch (LPException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                //showStrategy(1,strategyP1,labelsP1);
                //showStrategy(2,strategyP2,labelsP2);
                try{
                    this.provideStrategy(myStrategy);
                    playComplete = true;
                } catch (InvalidStrategyException e) {
                    System.err.println("Invalid strategy: " + e.getMessage());;
                    e.printStackTrace(System.err);
                }
            }
        }

    }

    public String showLabel(String label) {
        return label.substring(label.lastIndexOf(':')+1);
    }

    public void showMoves(int P, String[] labels) {
        System.out.println("Moves Player " + P + ":");
        for (int i = 0; i<labels.length; i++) System.out.println("   " + showLabel(labels[i]));
    }

    public void showUtility(int P, int n1, int n2, int[][] M) {
        System.out.println("Utility Player " + P + ":");
        for (int i = 0; i<n1; i++) {
            for (int j = 0; j<n2; j++) System.out.print("| " + M[i][j] + " ");
            System.out.println("|");
        }
    }


    public void setStrategyAgainstMaxMin(String[] labelsP1, String[] labelsP2, int[][] U1, int[][] U2, PlayStrategy myStrategy) throws LPException {
        /******PLAYING FOR P1***********/

        double [] movesP1 = getMoves(U1, U2);

        System.out.println("P1 Playing");
        //Playing for P1
        for(int i = 0; i < labelsP1.length; i++) {
            myStrategy.put(labelsP1[i], movesP1[i]);
        }

        /******PLAYING FOR P2***********/
        System.out.println("P2 Playing");

        double [] movesP2 = getMoves(transpose(U2), transpose(U1));

        //Playing for P2
        for(int i = 0; i < labelsP2.length; i++) {
            myStrategy.put(labelsP2[i], movesP2[i]);
        }
    }

    public static double[] getMoves(int[][] U1, int[][] U2){
        //As P1, I want to find out the maxmin outcome from P2
        //Find the mins
        int[] minsp2 = new int[U1[0].length];
        Arrays.fill(minsp2, Integer.MAX_VALUE);
        for (int i = 0; i < U2[0].length; i++){
            for (int j = 0; j < U2.length; j++){
                minsp2[i] = min(minsp2[i], U2[j][i]);
            }
        }

        //Get the maxmin
        int maxp2 = Integer.MIN_VALUE;
        List<Integer> maxIndexp2 = new ArrayList<>();
        for (int i = 0; i < minsp2.length; i++){
            if (minsp2[i] > maxp2){
                maxp2 = minsp2[i];
                maxIndexp2.clear();
                maxIndexp2.add(i);
            } else if (minsp2[i] == maxp2){
                maxIndexp2.add(i);
            }
        }

        int moveIndexp2 = -1;

        //There's more than a possible maxmin strat
        if (maxIndexp2.size() > 1){
            //Find which action may get me the highest utility
            int maxAux = Integer.MIN_VALUE;
            for (Integer t : maxIndexp2){
                for (int j = 0; j < U2.length; j++){
                    if (U2[j][t] > maxAux){
                        maxAux = U2[j][t];
                        moveIndexp2 = t;
                    }
                }
            }
        } else {
            moveIndexp2 = maxIndexp2.get(0);
        }

        System.out.println("moveIndex: "+moveIndexp2);

        //Now as I know what P1 is playing, I want to play the action that maximizes my utility based on it
        int p1BR = Integer.MIN_VALUE;
        int p1BRIndex = -1;
        for (int i = 0; i < U1.length; i++){
            System.out.println("BR: "+U1[i][moveIndexp2]);
            if (U1[i][moveIndexp2] > p1BR){
                p1BRIndex = i;
                p1BR = U1[i][moveIndexp2];
            }
        }

        double[] movesP1 = new double[U1.length];
        movesP1[p1BRIndex] = 1.0;

        return movesP1;
    }

    public static int[][] transpose(int[][] u) {
        int lin = u.length;
        int col = u[0].length;
        int[][] t = new int[col][lin];
        for (int i = 0; i<lin; i++)
            for (int j = 0; j<col; j++) t[j][i] = u[i][j];
        return t;
    }

    public void showStrategy(int P, double[] strategy, String[] labels) {
        System.out.println("Strategy Player " + P + ":");
        for (int i = 0; i<labels.length; i++) System.out.println("   " + strategy[i] + ":" + showLabel(labels[i]));
    }

}
